"""
Уявіть що ми з Вами пишемо застосунок який допоможе офлайн-бібліотеці мати онлайн-представництво.
І нам необхідно описати сутності, якими оперує онлайн-бібліотека, - книжки.

- Кожна книжка має атрибути:
    - Назву
    - Опис
    - Мову книги
    - Список авторів (!!)
    - Список літературних жанрів до яких вона віднесена (!!)
    - Рік видання
    - ISBN (хто не знає що це таке - загугліть)
- Вам треба описати:
    - Клас книги
    - Клас автора (поля: first_name, last_name, year_of_birth)
    - Класс жанра книги (поля: назва жанру, опис жанру)
- Клас книги використовує класи автор і жанр для визначення своїх полів.
- Реалізуйте методи __str__ і __repr__ для всіх класів.
- Реалізуйте метод __eq__
    - для авторів (автори вважаються однаковими, якщо всі атрибути однакові) і
    - для книги (книги вважаються однаковими, якщо однакові назви і мають тих самих авторів).
- Всі методи повинні бути документовані і анотовані.

"""
from typing import List, Optional


class Book:
    def __init__(
            self,
            name: str,
            authors: List['Author'],
            description: Optional[str] = None,
            language: Optional[str] = None,
            genres: Optional[List['Genre']] = None,
            issued: Optional[int] = None,
            isbn: Optional[str] = None
    ) -> None:
        self.name = name
        self.authors = authors
        self.description = description
        self.language = language
        self.genres = genres
        self.issued = issued
        self.isbn = isbn

    def __str__(self):
        """
        >>> zelazny = Author('Roger', 'Zelazny')
        >>> saberhagen = Author('Fred', 'Saberhagen')
        >>> bk1 = Book('Nine Princes in Amber', [zelazny]); print(bk1)
        Nine Princes in Amber by Roger Zelazny
        >>> bk2 = Book('Coils', [zelazny, saberhagen]); print(bk2)
        Coils by Roger Zelazny, Fred Saberhagen

        """
        authors_str = ', '.join(f"{author!s}" for author in self.authors)
        return f"{self.name} by {authors_str}"

    def __repr__(self):
        ...

    def __eq__(self, other: 'Book') -> bool:
        if not isinstance(other, Book):
            raise TypeError(
                f"'==' not supported between instances of 'Book' and '{type(other)}'"
            )
        return all([
            self.name == other.name,
            set(self.authors) == set(other.authors)
        ])


class Author:
    def __init__(
            self,
            first_name: str,
            last_name: str,
            year_of_birth: Optional[int] = None
    ):
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self):
        """
        >>> at1 = Author('Arthur', 'Clarke', 1917); print(at1)
        Arthur Clarke
        >>> at2 = Author('Ray', 'Bradbury'); print(at2)
        Ray Bradbury

        """
        return f"{self.first_name} {self.last_name}"

    def __repr__(self):
        """
        >>> at1 = Author('Arthur', 'Clarke', 1917); at1
        Author('Arthur', 'Clarke', 1917)
        >>> at2 = Author('Ray', 'Bradbury'); at2
        Author('Ray', 'Bradbury')

        """
        return (
            f"Author('{self.first_name}', '{self.last_name}'"
            + ("" if self.year_of_birth is None else f", {self.year_of_birth}")
            + ")"
        )

    def __eq__(self, other: 'Author') -> bool:
        if not isinstance(other, Author):
            raise TypeError(
                f"'==' not supported between instances of 'Author' and '{type(other)}'"
            )
        return all([
            self.first_name == other.first_name,
            self.last_name == other.last_name,
            (self.year_of_birth == other.year_of_birth if (
                self.year_of_birth is not None
                and other.year_of_birth is not None
            ) else True)
        ])

    def __hash__(self):
        return hash((self.first_name, self.last_name, self.year_of_birth))


class Genre:
    def __init__(
            self,
            name: str,
            description: Optional[str] = None
    ) -> None:
        self.name = name
        self.description = description

    def __str__(self) -> str:
        """
        >>> gr1 = Genre('sci-fi', 'Science Fiction'); print(gr1)
        sci-fi
        >>> gr2 = Genre('non-fiction'); print(gr2)
        non-fiction

        """
        return self.name

    def __repr__(self):
        """
        >>> gr1 = Genre('sci-fi', 'Science Fiction'); gr1
        Genre('sci-fi', 'Science Fiction')
        >>> gr2 = Genre('non-fiction'); gr2
        Genre('non-fiction')

        """
        return (
            f"Genre('{self.name}'"
            + ("" if self.description is None else f", '{self.description}'")
            + ")"
        )


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
