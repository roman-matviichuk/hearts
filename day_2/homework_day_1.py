"""
---
1.
---
Написати функцію яка приймає рядок і повертає словник у якому
ключами є всі символи, які зустрічаються в цьому рядку,
а значення - відповідні вірогідності зустріти цей символ в цьому рядку.

- Код повинен бути структурований за допомогою конструкції if name == "main":,
- всі аргументи і значення, що функція повертає, повинні бути типізовані,
- функція має рядок документації

---
2.
---
За допомогою тернарного оператора релізувати логіку:

- є параметри x та у,
- якщо x < y - друкуємо x + y,
- якщо x == y - друкуємо 0,
- якщо x > y - друкуємо x - y,
- якщо x == 0 та y == 0 друкуємо "game over
"""


def get_character_frequency(input_str: str) -> dict:
    """
    Take a string as input and return a dictionary where

    - key is a character from the string,
    - value is frequency of occurrence of the character in the string

    >>> get_character_frequency("")
    {}
    >>> get_character_frequency("a")
    {'a': 1.0}
    >>> get_character_frequency("11")
    {'1': 1.0}
    >>> d1 = get_character_frequency("mama"); d1['m'], d1['a']
    (0.5, 0.5)
    >>> d2 = get_character_frequency("onion"); d2['o'], d2['n'], d2['i']
    (0.4, 0.4, 0.2)
    """
    total = len(input_str)
    characters = set(input_str)
    output_dict = {
        char: input_str.count(char) / total
        for char in characters
    }
    return output_dict


def evaluate_ternary_operator(x: int, y: int) -> str:
    """
    >>> evaluate_ternary_operator(1, 2)
    '3'
    >>> evaluate_ternary_operator(2, 1)
    '1'
    >>> evaluate_ternary_operator(0, 0)
    'game over'
    >>> evaluate_ternary_operator(1, 1)
    '0'
    """
    return str(
        x + y if x < y else
        x - y if x > y else
        "game over" if x == 0 and y == 0 else
        0
    )


if __name__ == "__main__":

    import doctest
    doctest.testmod(verbose=False)
