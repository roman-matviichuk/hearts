# Game of Hearts

## About
This is part of the CyberBionic Systematics [Marathon](https://edu.cbsystematics.com/ua/webinars/hearts-in-python). 

The aim of the project, in addition to creating a card game [Hearts](https://bicyclecards.com/how-to-play/hearts/), is a theoretical and practical understanding of

- OOP in Python
- Type Annotations

## Installation
TODO

## Usage
TODO

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## Project status
Ongoing
