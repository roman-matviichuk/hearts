"""
Модифікувати клас книги з ДЗ 3 таким чином, щоб параметри,
які ми передаємо при ініціалізації екземпляру були:

- name, language, year - обов'язковими
- автори - передавались не списком а як звичайні позиційні аргументи
- опис книги, isbn, genres - необов'язкові параметри і лише як ключові
  (підказки тут https://docs.python.org/3/tutorial/controlflow.html#special-parameters)
- створіть метод у книги який повертає вік книги в роках (відносно поточного)
  (підказки тут - https://docs.python.org/3/library/datetime.html)

"""
from datetime import datetime
from typing import List, Optional


class Book:
    def __init__(
            self,
            name: str,
            language: str,
            year: int,
            *authors: 'Author',
            description: Optional[str] = None,
            genres: Optional[List['Genre']] = None,
            isbn: Optional[str] = None
    ) -> None:
        self.name = name
        self.language = language
        self.year = year
        self.authors = list(authors)
        self.description = description
        self.genres = genres
        self.isbn = isbn

    def get_age(self):
        """
        Find the age of the book in years

        >>> zelazny = Author('Roger', 'Zelazny')
        >>> Book('Nine Princes in Amber', 'EN', 1970, zelazny).get_age()
        53
        """
        return datetime.now().year - self.year

    def __str__(self):
        """
        >>> zelazny = Author('Roger', 'Zelazny')
        >>> saberhagen = Author('Fred', 'Saberhagen')
        >>> print(Book('Nine Princes in Amber', 'EN', 1970, zelazny))
        Nine Princes in Amber by Roger Zelazny
        >>> print(Book('Coils', 'EN', 1982, zelazny, saberhagen))
        Coils by Roger Zelazny, Fred Saberhagen
        """
        authors_str = ', '.join(f"{author!s}" for author in self.authors)
        return f"{self.name} by {authors_str}"

    def __repr__(self):
        """
        >>> zelazny = Author('Roger', 'Zelazny')
        >>> saberhagen = Author('Fred', 'Saberhagen')
        >>> Book('Nine Princes in Amber', 'EN', 1970, zelazny, isbn='978-0380014309')
        Book('Nine Princes in Amber', 'EN', 1970, Author('Roger', 'Zelazny'), \
description=None, genres=None, isbn='978-0380014309')
        >>> Book('Coils', 'EN', 1982, zelazny, saberhagen)
        Book('Coils', 'EN', 1982, Author('Roger', 'Zelazny'), Author('Fred', 'Saberhagen'), \
description=None, genres=None, isbn=None
        """
        return (
            f"Book('{self.name}', '{self.language}', {self.year}, {str(self.authors)[1:-1]}, "
            f"description=" + ('None' if self.description is None else f"'{self.description}'")
            + ", genres=" + ('None' if self.genres is None else f"{self.genres}")
            + ", isbn=" + ('None' if self.isbn is None else f"'{self.isbn}')")
        )

    def __eq__(self, other: 'Book') -> bool:
        if not isinstance(other, Book):
            raise TypeError(
                f"'==' not supported between instances of 'Book' and '{type(other)}'"
            )
        return all([
            self.name == other.name,
            set(self.authors) == set(other.authors)
        ])


class Author:
    def __init__(
            self,
            first_name: str,
            last_name: str,
            year_of_birth: Optional[int] = None
    ):
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self):
        """
        >>> print(Author('Arthur', 'Clarke', 1917))
        Arthur Clarke
        >>> print(Author('Ray', 'Bradbury'))
        Ray Bradbury
        """
        return f"{self.first_name} {self.last_name}"

    def __repr__(self):
        """
        >>> Author('Arthur', 'Clarke', 1917)
        Author('Arthur', 'Clarke', 1917)
        >>> Author('Ray', 'Bradbury')
        Author('Ray', 'Bradbury')
        """
        return (
            f"Author('{self.first_name}', '{self.last_name}'"
            + ("" if self.year_of_birth is None else f", {self.year_of_birth}")
            + ")"
        )

    def __eq__(self, other: 'Author') -> bool:
        if not isinstance(other, Author):
            raise TypeError(
                f"'==' not supported between instances of 'Author' and '{type(other)}'"
            )
        return all([
            self.first_name == other.first_name,
            self.last_name == other.last_name,
            (self.year_of_birth == other.year_of_birth if (
                self.year_of_birth is not None
                and other.year_of_birth is not None
            ) else True)
        ])

    def __hash__(self):
        return hash((self.first_name, self.last_name, self.year_of_birth))


class Genre:
    def __init__(
            self,
            name: str,
            description: Optional[str] = None
    ) -> None:
        self.name = name
        self.description = description

    def __str__(self) -> str:
        """
        >>> print(Genre('sci-fi', 'Science Fiction'))
        sci-fi
        >>> print(Genre('non-fiction'))
        non-fiction
        """
        return self.name

    def __repr__(self):
        """
        >>> Genre('sci-fi', 'Science Fiction')
        Genre('sci-fi', 'Science Fiction')
        >>> Genre('non-fiction')
        Genre('non-fiction')
        """
        return (
            f"Genre('{self.name}'"
            + ("" if self.description is None else f", '{self.description}'")
            + ")"
        )


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
